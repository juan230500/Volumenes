"""
******************************************************************
Instituto: Tecnológico de Costa Rica
Carrera :Ing. Computadores 
Programa : Calculadora de Área y de Volumen para esferas y pirámides cuadradas
Autores : Juan Pablo Alvarado y Mariana Vargas Ramírez
Version : 1.0
Ult.Fecha de mod: 24/4/18
Entradas : radio, altura y lado
Restricciones: r>=0 and h>=0 and l>=0
Salidas: Volumen y Área
******************************************************************"""

def vol_e(r):
      """
******************************************************************
Instituto: Tecnológico de Costa Rica
Carrera :Ing. Computadores 
Programa : Calculadora de Área Volumen para esferas
Autores : Juan Pablo Alvarado y Mariana Vargas Ramírez
Version : 1.0
Ult.Fecha de mod: 24/4/18
Entradas : radio
Restricciones: r>=0 and h>=0 and l>=0
Salidas: Volumen y Área
******************************************************************"""
      from math import pi
      return (4*r**3*pi)/3

def vol_p(l,h):
      """
******************************************************************
Instituto: Tecnológico de Costa Rica
Carrera :Ing. Computadores 
Programa : Calculadora de Volumen para pirámides cuadradas
Autores : Juan Pablo Alvarado y Mariana Vargas Ramírez
Version : 1.0
Ult.Fecha de mod: 24/4/18
Entradas : altura y lado
Restricciones: r>=0 and h>=0 and l>=0
Salidas: Volumen y Área
******************************************************************"""
      return l**2*h/3#lul


